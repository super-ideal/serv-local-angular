# Servidor Local Angular
Embora qualquer web server popular pode ser usado para hospedagem de uma aplicação Angular 2+, aqui usamos o próprio NodeJS em conjunto com o excelente framework express, que permite disponibilizar um código HTML na web.

Uma das vantagens em utilizar o express, é que poderemos visualizar o código a ser colocado em produção localmente, integrado com o código do projeto.

## Heroku
Por fim, utilizaremos o Heroku, que é uma das melhores hospedagens na nuvem para iniciar um projeto, sendo que ele possui uma versão gratuita que para sua utilização basta realizar um cadastro sem complicações.

- git push heroku master (Envia novo push para a aplicação)

### Links Úteis
- [Angular - Heroku deploy](http://kazale.com/angular-2-heroku-deploy/)
