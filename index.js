'use strict';
/**
 * Arquivo para criação de um servidor local 
 * utilizado para executar o código de produção.
 *
 */
 
const path = require('path');
const express = require('express');
const app = express();

app.set('port', (process.env.PORT || 3000));
app.use(express.static(path.join(__dirname, 'dist')));

// redireciona todas as requições para o Angular
app.get('*', (req, res, next) => {
  res.status(200).sendFile(
    path.join(__dirname, 'dist', 'index.html')
  );
});
 
app.listen(app.get('port'), function() {
  console.log('Node executando na porta ', app.get('port'));
});